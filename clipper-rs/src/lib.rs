use clipper_sys::interface::{IntPoint, PathBuf, PathsBuf};
use clipper_sys::Path as CPath;
use clipper_sys::Paths as CPaths;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum PointPolygon {
    /// The point is outside the polygon
    Outside,

    /// The point is on the edge of the polygon
    On,

    /// The point within the polygon
    In,
}

impl PointPolygon {
    pub fn in_or_on(&self) -> bool {
        match self {
            PointPolygon::Outside => false,
            PointPolygon::On => true,
            PointPolygon::In => true,
        }
    }
}

impl From<i32> for PointPolygon {
    fn from(i: i32) -> Self {
        if i < 0 {
            PointPolygon::On
        } else if i > 0 {
            PointPolygon::In
        } else {
            PointPolygon::Outside
        }
    }
}

pub enum FillType {
    EvenOdd,
    NonZero,
    Positive,
    Negative,
}

impl From<clipper_sys::ClipperLib_PolyFillType> for FillType {
    fn from(f: clipper_sys::ClipperLib_PolyFillType) -> Self {
        match f {
            clipper_sys::ClipperLib_PolyFillType_pftEvenOdd => FillType::EvenOdd,
            clipper_sys::ClipperLib_PolyFillType_pftNonZero => FillType::NonZero,
            clipper_sys::ClipperLib_PolyFillType_pftPositive => FillType::Positive,
            clipper_sys::ClipperLib_PolyFillType_pftNegative => FillType::Negative,
            e => panic!("Got an unknown PolyFillType: {}", e),
        }
    }
}

impl From<FillType> for clipper_sys::ClipperLib_PolyFillType {
    fn from(f: FillType) -> Self {
        match f {
            FillType::EvenOdd => clipper_sys::ClipperLib_PolyFillType_pftEvenOdd,
            FillType::NonZero => clipper_sys::ClipperLib_PolyFillType_pftNonZero,
            FillType::Positive => clipper_sys::ClipperLib_PolyFillType_pftPositive,
            FillType::Negative => clipper_sys::ClipperLib_PolyFillType_pftNegative,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Point {
    pub x: i64,
    pub y: i64,
}

impl Point {
    pub fn new(x: i64, y: i64) -> Point {
        Point { x, y }
    }
}

impl From<IntPoint> for Point {
    fn from(p: IntPoint) -> Self {
        Point { x: p.X, y: p.Y }
    }
}

impl From<&IntPoint> for Point {
    fn from(p: &IntPoint) -> Self {
        Point { x: p.X, y: p.Y }
    }
}

impl From<Point> for IntPoint {
    fn from(p: Point) -> Self {
        IntPoint { X: p.x, Y: p.y }
    }
}

impl From<&Point> for IntPoint {
    fn from(p: &Point) -> Self {
        IntPoint { X: p.x, Y: p.y }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Path {
    points: Vec<Point>,
}

impl Path {
    pub fn new() -> Path {
        Path { points: Vec::new() }
    }

    pub fn from_points(points: Vec<Point>) -> Path {
        Path { points }
    }

    /// Create a `Path` from a `PathBuf`.
    ///
    /// # Safety
    ///
    /// There must not be any c paths that reference the `PathBuf` around.
    unsafe fn from_buf(buf: PathBuf) -> Path {
        let out_points = buf.into_points().iter().map(From::from).collect();
        Path::from_points(out_points)
    }

    /// Create a `Path` from a c path
    ///
    /// # Safety
    ///
    /// The path must have been allocated from c and point to valid points
    unsafe fn from_c_path(c_path: CPath) -> Path {
        let buf = PathBuf::from_c_path(c_path);
        Path::from_buf(buf)
    }

    pub fn points(&self) -> &[Point] {
        self.points.as_slice()
    }

    pub fn into_points(self) -> Vec<Point> {
        self.points
    }

    pub fn iter(&self) -> impl Iterator<Item = &Point> {
        self.points.iter()
    }

    fn buf(&self) -> PathBuf {
        PathBuf::new(self.points.iter().map(Into::into).collect())
    }

    pub fn area(&self) -> f64 {
        let mut buf = self.buf();
        unsafe { clipper_sys::area(buf.as_path()) }
    }

    pub fn orientation(&self) -> bool {
        let mut buf = self.buf();
        unsafe { clipper_sys::orientation(buf.as_path()) }
    }

    pub fn point_in_polygon(&self, point: Point) -> PointPolygon {
        let mut buf = self.buf();
        unsafe { clipper_sys::point_in_polygon(point.into(), buf.as_path()).into() }
    }

    pub fn reverse(&mut self) {
        *self = unsafe { self.map_to_path(|path| clipper_sys::reverse_path(path)) };
    }

    pub fn clean(&mut self, distance: f64) {
        *self = unsafe { self.map_to_path(|path| clipper_sys::clean_polygon(path, distance)) };
    }

    pub fn simplify(&self, fill_type: FillType) -> Paths {
        unsafe { self.map_to_paths(|path| clipper_sys::simplify_polygon(path, fill_type.into())) }
    }

    pub fn minkowski_sum(&self, pattern: Path, path_is_closed: bool) -> Paths {
        let mut pattern_buf = pattern.buf();
        unsafe {
            self.map_to_paths(|path| {
                clipper_sys::minkowski_sum(pattern_buf.as_path(), path, path_is_closed)
            })
        }
    }

    pub fn minkowski_diff(&mut self, poly2: Path) -> Paths {
        let mut poly2_buf = poly2.buf();
        unsafe { self.map_to_paths(|path| clipper_sys::minkowski_diff(path, poly2_buf.as_path())) }
    }

    /// Update the points with the result of a c function, passed as a closure
    ///
    /// # Safety
    ///
    /// The Path passed as an argument must not be freed by `f`.
    /// The Path returned must be allocated from c, and freeable from c.
    unsafe fn map_to_path<F: FnOnce(CPath) -> CPath>(&self, f: F) -> Path {
        let mut buf = self.buf();
        let out_path = f(buf.as_path());
        Path::from_c_path(out_path)
    }

    unsafe fn map_to_paths<F: FnOnce(CPath) -> CPaths>(&self, f: F) -> Paths {
        let mut buf = self.buf();
        let out_paths = f(buf.as_path());
        Paths::from_c_paths(out_paths)
    }
}

#[test]
fn test_point_in_polygon() {
    let path = Path::from_points(vec![
        Point::new(0, 0),
        Point::new(10, 0),
        Point::new(10, 10),
        Point::new(0, 10),
    ]);

    assert_eq!(path.point_in_polygon(Point::new(5, 5)), PointPolygon::In)
}

impl From<Path> for Paths {
    fn from(path: Path) -> Self {
        Paths::from_paths(vec![path])
    }
}

#[derive(Clone, Debug, Default)]
pub struct Paths {
    paths: Vec<Path>,
}

impl Paths {
    pub fn new() -> Paths {
        Paths { paths: Vec::new() }
    }

    pub fn from_paths(paths: Vec<Path>) -> Paths {
        Paths { paths }
    }

    pub fn from_points(points: Vec<Vec<Point>>) -> Paths {
        Paths {
            paths: points.into_iter().map(|p| Path::from_points(p)).collect(),
        }
    }

    unsafe fn from_buf(buf: PathsBuf) -> Paths {
        let points = buf
            .into_points()
            .iter()
            .map(|p| p.iter().map(From::from).collect())
            .collect();
        Paths::from_points(points)
    }

    unsafe fn from_c_paths(c_paths: CPaths) -> Paths {
        let buf = PathsBuf::from_c_paths(c_paths);
        Paths::from_buf(buf)
    }

    pub fn paths(&self) -> &[Path] {
        self.paths.as_slice()
    }

    pub fn into_paths(self) -> Vec<Path> {
        self.paths
    }

    pub fn into_points(self) -> Vec<Vec<Point>> {
        self.paths.into_iter().map(|p| p.into_points()).collect()
    }

    fn buf(&self) -> PathsBuf {
        PathsBuf::new(
            self.clone()
                .into_points()
                .into_iter()
                .map(|p| p.into_iter().map(Into::into).collect())
                .collect(),
        )
    }

    pub fn area(&self) -> f64 {
        self.paths.iter().map(Path::area).sum()
    }

    pub fn empty(&self) -> bool {
        self.paths
            .iter()
            .map(|path| path.points.len())
            .sum::<usize>()
            == 0
    }

    pub fn reverse(&mut self) {
        *self = unsafe { self.map_to_paths(|paths| clipper_sys::reverse_paths(paths)) };
    }

    pub fn clean(&mut self, distance: f64) {
        *self = unsafe { self.map_to_paths(|paths| clipper_sys::clean_polygons(paths, distance)) };
    }

    pub fn simplify(&mut self, fill_type: FillType) {
        *self = unsafe {
            self.map_to_paths(|paths| clipper_sys::simplify_polygons(paths, fill_type.into()))
        };
    }

    pub fn minkowski_sum(&mut self, pattern: Path, path_is_closed: bool) {
        let mut pattern_buf = pattern.buf();
        *self = unsafe {
            self.map_to_paths(|paths| {
                clipper_sys::minkowski_sums(pattern_buf.as_path(), paths, path_is_closed)
            })
        };
    }

    unsafe fn map_to_paths<F: FnOnce(CPaths) -> CPaths>(&self, f: F) -> Paths {
        let mut buf = self.buf();
        let out_paths = f(buf.as_paths());
        Paths::from_c_paths(out_paths)
    }
}

pub enum PolyType {
    Subject,
    Clip,
}

impl From<PolyType> for clipper_sys::ClipperLib_PolyType {
    fn from(poly_type: PolyType) -> Self {
        match poly_type {
            PolyType::Subject => clipper_sys::ClipperLib_PolyType_ptSubject,
            PolyType::Clip => clipper_sys::ClipperLib_PolyType_ptClip,
        }
    }
}

pub struct Rect {
    pub left: i64,
    pub top: i64,
    pub right: i64,
    pub bottom: i64,
}

impl From<Rect> for clipper_sys::ClipperLib_IntRect {
    fn from(rect: Rect) -> Self {
        clipper_sys::ClipperLib_IntRect {
            left: rect.left,
            top: rect.top,
            right: rect.top,
            bottom: rect.bottom,
        }
    }
}

impl From<clipper_sys::ClipperLib_IntRect> for Rect {
    fn from(rect: clipper_sys::ClipperLib_IntRect) -> Self {
        Rect {
            left: rect.left,
            top: rect.top,
            right: rect.top,
            bottom: rect.bottom,
        }
    }
}

pub enum ClipType {
    Intersection,
    Union,
    Difference,
    Xor,
}

impl From<ClipType> for clipper_sys::ClipperLib_ClipType {
    fn from(clip_type: ClipType) -> Self {
        match clip_type {
            ClipType::Intersection => clipper_sys::ClipperLib_ClipType_ctIntersection,
            ClipType::Union => clipper_sys::ClipperLib_ClipType_ctUnion,
            ClipType::Difference => clipper_sys::ClipperLib_ClipType_ctDifference,
            ClipType::Xor => clipper_sys::ClipperLib_ClipType_ctXor,
        }
    }
}

pub struct Clipper {
    c: clipper_sys::Clipper,
}

impl Clipper {
    pub fn new() -> Clipper {
        let c = unsafe { clipper_sys::make_clipper() };
        Clipper { c }
    }

    pub fn preserve_collinear(&mut self, value: bool) {
        unsafe { clipper_sys::clipper_preserve_collinear(self.c, value) };
    }

    pub fn reverse_solution(&mut self, value: bool) {
        unsafe { clipper_sys::clipper_reverse_solution(self.c, value) };
    }

    pub fn strictly_simple(&mut self, value: bool) {
        unsafe { clipper_sys::clipper_strictly_simple(self.c, value) };
    }

    pub fn add_path(&mut self, path: Path, poly_type: PolyType, closed: bool) {
        let mut buf = path.buf();
        unsafe { clipper_sys::clipper_add_path(self.c, buf.as_path(), poly_type.into(), closed) };
    }

    pub fn with_path(mut self, path: Path, poly_type: PolyType, closed: bool) -> Clipper {
        self.add_path(path, poly_type, closed);
        self
    }

    pub fn add_paths(&mut self, paths: Paths, poly_type: PolyType, closed: bool) {
        let mut buf = paths.buf();
        unsafe { clipper_sys::clipper_add_paths(self.c, buf.as_paths(), poly_type.into(), closed) };
    }

    pub fn with_paths(mut self, paths: Paths, poly_type: PolyType, closed: bool) -> Clipper {
        self.add_paths(paths, poly_type, closed);
        self
    }

    pub fn clean(&mut self) {
        unsafe { clipper_sys::clipper_clear(self.c) };
    }

    pub fn bounds(&self) -> Rect {
        unsafe { clipper_sys::clipper_get_bounds(self.c).into() }
    }

    pub fn execute(
        &self,
        clip_type: ClipType,
        subj_fill_type: FillType,
        clip_fill_type: FillType,
    ) -> Result<Paths, ()> {
        let result = unsafe {
            clipper_sys::clipper_execute_paths(
                self.c,
                clip_type.into(),
                subj_fill_type.into(),
                clip_fill_type.into(),
            )
        };
        if result.success {
            unsafe { Ok(Paths::from_c_paths(result.paths)) }
        } else {
            Err(())
        }
    }

    pub fn execute_tree(
        &self,
        clip_type: ClipType,
        subj_fill_type: FillType,
        clip_fill_type: FillType,
    ) -> Result<(Paths, Paths), ()> {
        let result = unsafe {
            clipper_sys::clipper_execute_poly_tree(
                self.c,
                clip_type.into(),
                subj_fill_type.into(),
                clip_fill_type.into(),
            )
        };
        if result.success {
            unsafe {
                Ok((
                    Paths::from_c_paths(result.open_paths),
                    Paths::from_c_paths(result.closed_paths),
                ))
            }
        } else {
            Err(())
        }
    }
}

impl Drop for Clipper {
    fn drop(&mut self) {
        unsafe { clipper_sys::free_clipper(self.c) }
    }
}

pub enum JoinType {
    Square,
    Round,
    Miter,
}

impl From<JoinType> for clipper_sys::ClipperLib_JoinType {
    fn from(join_type: JoinType) -> Self {
        match join_type {
            JoinType::Square => clipper_sys::ClipperLib_JoinType_jtSquare,
            JoinType::Round => clipper_sys::ClipperLib_JoinType_jtRound,
            JoinType::Miter => clipper_sys::ClipperLib_JoinType_jtMiter,
        }
    }
}

pub enum EndType {
    ClosedPolygon,
    ClosedLine,
    OpenButt,
    OpenSquare,
    OpenRound,
}

impl From<EndType> for clipper_sys::ClipperLib_EndType {
    fn from(end_type: EndType) -> Self {
        match end_type {
            EndType::ClosedPolygon => clipper_sys::ClipperLib_EndType_etClosedPolygon,
            EndType::ClosedLine => clipper_sys::ClipperLib_EndType_etClosedLine,
            EndType::OpenButt => clipper_sys::ClipperLib_EndType_etOpenButt,
            EndType::OpenSquare => clipper_sys::ClipperLib_EndType_etOpenSquare,
            EndType::OpenRound => clipper_sys::ClipperLib_EndType_etOpenRound,
        }
    }
}

pub struct ClipperOffset {
    c: clipper_sys::ClipperOffset,
}

impl ClipperOffset {
    pub fn new(miter_limit: f64, round_precision: f64) -> ClipperOffset {
        let c = unsafe { clipper_sys::make_clipper_offset(miter_limit, round_precision) };
        ClipperOffset { c }
    }

    pub fn add_path(&mut self, path: Path, join_type: JoinType, end_type: EndType) {
        let mut buf = path.buf();
        unsafe {
            clipper_sys::clipper_offset_add_path(
                self.c,
                buf.as_path(),
                join_type.into(),
                end_type.into(),
            )
        };
    }

    pub fn with_path(
        mut self,
        path: Path,
        join_type: JoinType,
        end_type: EndType,
    ) -> ClipperOffset {
        self.add_path(path, join_type, end_type);
        self
    }

    pub fn add_paths(&mut self, paths: Paths, join_type: JoinType, end_type: EndType) {
        let mut buf = paths.buf();
        unsafe {
            clipper_sys::clipper_offset_add_paths(
                self.c,
                buf.as_paths(),
                join_type.into(),
                end_type.into(),
            )
        };
    }

    pub fn with_paths(
        mut self,
        paths: Paths,
        join_type: JoinType,
        end_type: EndType,
    ) -> ClipperOffset {
        self.add_paths(paths, join_type, end_type);
        self
    }

    pub fn clear(&mut self) {
        unsafe { clipper_sys::clipper_offset_clear(self.c) };
    }

    pub fn execute(&self, delta: f64) -> Paths {
        let paths = unsafe { clipper_sys::clipper_offset_execute_paths(self.c, delta) };
        unsafe { Paths::from_c_paths(paths) }
    }

    pub fn execute_tree(&self, delta: f64) -> (Paths, Paths) {
        let result = unsafe { clipper_sys::clipper_offset_execute_polytree(self.c, delta) };
        unsafe {
            (
                Paths::from_c_paths(result.open_paths),
                Paths::from_c_paths(result.closed_paths),
            )
        }
    }
}

impl Default for ClipperOffset {
    fn default() -> ClipperOffset {
        ClipperOffset::new(2.0, 0.25)
    }
}

impl Drop for ClipperOffset {
    fn drop(&mut self) {
        unsafe { clipper_sys::free_clipper_offset(self.c) };
    }
}
