use std::slice;

use crate::{free_path, free_paths, ClipperLib_IntPoint, Path, Paths};

pub type IntPoint = ClipperLib_IntPoint;

/// Holds a list of points that represent a path.
///
/// It is the owned variant of a `Path` that holds the points that a `Path` refers to.
/// `Path`s are used to interface with the C++ Clipper lib (with a c wrapper), and the
/// contents get copied out on the other side. `Path`s have raw pointers to the points,
/// so Rust's lifetimes don't really apply, but it is important that a `Path` is never
/// used without the backing points. `PathBuf` owns these points, and attempts to
/// prevent misuse. The `PathBuf` must live at least as long as any `Path` derived from
/// it.
///
/// Note that this `PathBuf` has nothing to do with the std `PathBuf`
pub struct PathBuf(Vec<IntPoint>);

impl PathBuf {
    /// Creates a new `PathBuf` from a list of points
    pub fn new(points: Vec<IntPoint>) -> PathBuf {
        PathBuf(points)
    }

    /// Copy the points referenced from the `Path` to create a new `PathBuf`.
    ///
    /// # Safety
    ///
    /// Make sure that the `Path` refers to valid points.
    pub unsafe fn from_path(path: Path) -> PathBuf {
        PathBuf(slice::from_raw_parts_mut(path.ptr, path.len as usize).to_vec())
    }

    /// Does the same as `from_path`, but calls a c function to free the
    /// referenced memory.
    ///
    /// # Safety
    ///
    /// This will do very bad things if it is used on a `Path` that did not come from c, and
    /// the Path will be invalid.
    pub unsafe fn from_c_path(path: Path) -> PathBuf {
        let buf = PathBuf(slice::from_raw_parts_mut(path.ptr, path.len as usize).to_vec());
        free_path(path);
        buf
    }

    /// Destroys this `PathBuf` to get the points back.
    ///
    /// # Safety
    ///
    /// There must not be any `Path`s that reference this `PathBuf` when this function is
    /// called, or they memory they reference may disappear.
    pub unsafe fn into_points(self) -> Vec<IntPoint> {
        self.0
    }

    /// Create a `Path` that references the points in this `PathBuf`. It is
    /// important that the `Path` has a lifetime shorter than the `PathBuf`
    /// it comes from.
    pub fn as_path(&mut self) -> Path {
        Path {
            ptr: self.0.as_mut_ptr(),
            len: self.0.len() as u64,
        }
    }
}

pub struct PathsBuf(Vec<Vec<IntPoint>>, Vec<Path>);

impl PathsBuf {
    /// Creates a new `PathsBuf` from a list of point lists
    pub fn new(mut points: Vec<Vec<IntPoint>>) -> PathsBuf {
        let paths = points
            .iter_mut()
            .map(|path_vec| Path {
                ptr: path_vec.as_mut_ptr(),
                len: path_vec.len() as u64,
            })
            .collect::<Vec<_>>();

        PathsBuf(points, paths)
    }

    pub unsafe fn from_paths(paths: Paths) -> PathsBuf {
        let raw_paths = slice::from_raw_parts_mut(paths.ptr, paths.len as usize);

        let points = raw_paths
            .iter()
            .map(|path| slice::from_raw_parts_mut(path.ptr, path.len as usize).to_vec())
            .collect::<Vec<_>>();

        Self::new(points)
    }

    pub unsafe fn from_c_paths(paths: Paths) -> PathsBuf {
        let raw_paths = slice::from_raw_parts_mut(paths.ptr, paths.len as usize);

        let points = raw_paths
            .iter()
            .map(|path| slice::from_raw_parts_mut(path.ptr, path.len as usize).to_vec())
            .collect::<Vec<_>>();

        free_paths(paths);

        Self::new(points)
    }

    pub unsafe fn into_points(self) -> Vec<Vec<IntPoint>> {
        self.0
    }

    pub fn as_paths(&mut self) -> Paths {
        Paths {
            ptr: self.1.as_mut_ptr(),
            len: self.1.len() as u64,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::slice;

    use super::IntPoint;

    use super::Path;
    use super::Paths;

    use super::free_path;
    use super::free_paths;

    use super::test_path_roundtrip;
    use super::test_paths_roundtrip;

    use super::minkowski_sum;

    use crate::interface::{PathBuf, PathsBuf};

    #[test]
    fn path_roundtrip() {
        let path_vec = vec![
            IntPoint { X: 0, Y: 0 },
            IntPoint { X: 2, Y: 0 },
            IntPoint { X: 2, Y: 2 },
            IntPoint { X: 0, Y: 2 },
        ];

        let mut pathbuf = PathBuf::new(path_vec.clone());

        let roundtripped_path = unsafe { test_path_roundtrip(pathbuf.as_path()) };

        let roundtripped_pathbuf = unsafe { PathBuf::from_c_path(roundtripped_path) };

        let roundtripped_points = unsafe { roundtripped_pathbuf.into_points() };

        assert_eq!(path_vec, roundtripped_points);
    }

    #[test]
    fn paths_roundtrip() {
        let paths_vec = vec![
            vec![
                IntPoint { X: 0, Y: 0 },
                IntPoint { X: 2, Y: 0 },
                IntPoint { X: 2, Y: 2 },
                IntPoint { X: 0, Y: 2 },
            ],
            vec![
                IntPoint { X: 1, Y: 1 },
                IntPoint { X: 2, Y: 4 },
                IntPoint { X: 2, Y: 2 },
                IntPoint { X: 3, Y: 2 },
            ],
            vec![
                IntPoint { X: 6, Y: 3 },
                IntPoint { X: 2, Y: 2 },
                IntPoint { X: 9, Y: 3 },
                IntPoint { X: 2, Y: 4 },
            ],
        ];

        let mut pathsbuf = PathsBuf::new(paths_vec.clone());

        let roundtripped_paths = unsafe { test_paths_roundtrip(pathsbuf.as_paths()) };

        let roundtripped_pathsbuf = unsafe { PathsBuf::from_c_paths(roundtripped_paths) };

        let roundtripped_points = unsafe { roundtripped_pathsbuf.into_points() };

        assert_eq!(paths_vec, roundtripped_points);
    }

    #[test]
    fn minkowski() {
        let mut path = PathBuf::new(vec![
            IntPoint { X: 0, Y: 0 },
            IntPoint { X: 2, Y: 0 },
            IntPoint { X: 2, Y: 2 },
            IntPoint { X: 0, Y: 2 },
        ]);

        let mut pattern = PathBuf::new(vec![
            IntPoint { X: 0, Y: -1 },
            IntPoint { X: 1, Y: 0 },
            IntPoint { X: 0, Y: 1 },
            IntPoint { X: -1, Y: 0 },
        ]);

        let solution = unsafe {
            PathsBuf::from_c_paths(minkowski_sum(pattern.as_path(), path.as_path(), true))
        };

        assert_eq!(
            unsafe { solution.into_points() },
            vec![vec![
                IntPoint { X: -1, Y: 2 },
                IntPoint { X: -1, Y: 0 },
                IntPoint { X: 0, Y: -1 },
                IntPoint { X: 2, Y: -1 },
                IntPoint { X: 3, Y: 0 },
                IntPoint { X: 3, Y: 2 },
                IntPoint { X: 2, Y: 3 },
                IntPoint { X: 0, Y: 3 }
            ]]
        )
    }
}
