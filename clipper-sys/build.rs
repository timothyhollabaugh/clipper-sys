use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=clipper_c.h");
    println!("cargo:rerun-if-changed=clipper_c.cpp");

    cc::Build::new()
        .cpp(true)
        .file("clipper_c.cpp")
        .file("clipper/cpp/clipper.cpp")
        .compile("clipper");

    let bindings = bindgen::Builder::default()
        .header("clipper_c.h")
        .whitelist_function("free_path")
        .whitelist_function("free_paths")
        .whitelist_function("test_path_roundtrip")
        .whitelist_function("test_paths_roundtrip")
        .whitelist_function("minkowski_sum")
        .whitelist_function("area")
        .whitelist_function("orientation")
        .whitelist_function("point_in_polygon")
        .whitelist_function("reverse_path")
        .whitelist_function("reverse_paths")
        .whitelist_function("clean_polygon")
        .whitelist_function("clean_polygons")
        .whitelist_function("simplify_polygon")
        .whitelist_function("simplify_polygons")
        .whitelist_function("minkowski_sum")
        .whitelist_function("minkowski_sums")
        .whitelist_function("minkowski_diff")
        .whitelist_function("make_clipper")
        .whitelist_function("clipper_preserve_collinear")
        .whitelist_function("clipper_reverse_solution")
        .whitelist_function("clipper_strictly_simple")
        .whitelist_function("clipper_add_path")
        .whitelist_function("clipper_add_paths")
        .whitelist_function("clipper_clear")
        .whitelist_function("clipper_get_bounds")
        .whitelist_function("clipper_execute_paths")
        .whitelist_function("clipper_execute_poly_tree")
        .whitelist_function("free_clipper")
        .whitelist_function("make_clipper_offset")
        .whitelist_function("clipper_offset_add_path")
        .whitelist_function("clipper_offset_add_paths")
        .whitelist_function("clipper_offset_clear")
        .whitelist_function("clipper_offset_execute_paths")
        .whitelist_function("clipper_offset_execute_polytree")
        .whitelist_function("free_clipper_offset")
        .whitelist_type("Clipper")
        .whitelist_type("ClipperOffset")
        .opaque_type("Clipper")
        .opaque_type("ClipperOffset")
        .derive_partialeq(true)
        .clang_args(&["-x", "c++"])
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings");
}
