//
// Created by tim on 6/18/20.
//

#include <algorithm>

#include "clipper/cpp/clipper.hpp"

#include "clipper_c.h"

void free_path(Path path) {
    free(path.ptr);
}

void free_paths(Paths paths) {
    std::for_each(paths.ptr, paths.ptr + paths.len, &free_path);

    free(paths.ptr);
}

ClipperLib::Path path_to_clipper(Path path) {
    ClipperLib::Path clipper_path(path.ptr, path.ptr + path.len);
    return clipper_path;
}

Path path_from_clipper(ClipperLib::Path clipper_path) {
    ClipperLib::IntPoint *ptr = (ClipperLib::IntPoint *) malloc(clipper_path.size() * sizeof(ClipperLib::IntPoint));

    std::copy(clipper_path.begin(), clipper_path.end(), ptr);

    Path path = {
            .ptr = ptr,
            .len = clipper_path.size(),
    };

    return path;
}

ClipperLib::Paths paths_to_clipper(Paths paths) {
    ClipperLib::Paths clipper_paths;

    std::for_each(paths.ptr, paths.ptr + paths.len, [&](Path path) {
        ClipperLib::Path clipper_path = path_to_clipper(path);
        clipper_paths.push_back(clipper_path);
    });

    return clipper_paths;
}

Paths paths_from_clipper(ClipperLib::Paths clipper_paths) {
    Path *ptr = (Path *) malloc(clipper_paths.size() * sizeof(Path));

    std::transform(clipper_paths.begin(), clipper_paths.end(), ptr, &path_from_clipper);

    Paths paths = {
            .ptr = ptr,
            .len = clipper_paths.size(),
    };

    return paths;
}

Path test_path_roundtrip(Path path) {
    ClipperLib::Path clipper_path = path_to_clipper(path);
    return path_from_clipper(clipper_path);
}

Paths test_paths_roundtrip(Paths paths) {
    ClipperLib::Paths clipper_paths = paths_to_clipper(paths);
    return paths_from_clipper(clipper_paths);
}

double area(Path path) {
    return ClipperLib::Area(path_to_clipper(path));
}

bool orientation(Path poly) {
    return ClipperLib::Orientation(path_to_clipper(poly));
}

int point_in_polygon(ClipperLib::IntPoint pt, Path path) {
    return ClipperLib::PointInPolygon(pt, path_to_clipper(path));
}

Path reverse_path(Path p) {
    ClipperLib::Path path = path_to_clipper(p);
    ClipperLib::ReversePath(path);
    return path_from_clipper(path);
}

Paths reverse_paths(Paths p) {
    ClipperLib::Paths paths = paths_to_clipper(p);
    ClipperLib::ReversePaths(paths);
    return paths_from_clipper(paths);
}

Path clean_polygon(Path path, double distance) {
    ClipperLib::Path out;
    ClipperLib::CleanPolygon(path_to_clipper(path), out, distance);
    return path_from_clipper(out);
}

Paths clean_polygons(Paths paths, double distance) {
    ClipperLib::Paths out;
    ClipperLib::CleanPolygons(paths_to_clipper(paths), out, distance);
    return paths_from_clipper(out);
}

Paths simplify_polygon(Path poly, ClipperLib::PolyFillType filltype) {
    ClipperLib::Paths out;
    ClipperLib::SimplifyPolygon(path_to_clipper(poly), out, filltype);
    return paths_from_clipper(out);
}

Paths simplify_polygons(Paths polys, ClipperLib::PolyFillType filltype) {
    ClipperLib::Paths out;
    ClipperLib::SimplifyPolygons(paths_to_clipper(polys), out, filltype);
    return paths_from_clipper(out);
}

Paths minkowski_sum(Path pattern, Path path, bool path_is_closed) {
    ClipperLib::Paths out;

    ClipperLib::MinkowskiSum(
            path_to_clipper(pattern),
            path_to_clipper(path),
            out,
            path_is_closed
    );

    return paths_from_clipper(out);
}

Paths minkowski_sums(Path pattern, Paths paths, bool path_is_closed) {
    ClipperLib::Paths out;

    ClipperLib::MinkowskiSum(
            path_to_clipper(pattern),
            paths_to_clipper(paths),
            out,
            path_is_closed
    );

    return paths_from_clipper(out);
}

Paths minkowski_diff(Path poly1, Path poly2) {
    ClipperLib::Paths out;
    ClipperLib::MinkowskiDiff(path_to_clipper(poly1), path_to_clipper(poly2), out);
    return paths_from_clipper(out);
}

Clipper make_clipper() {
    ClipperLib::Clipper *clipper = new ClipperLib::Clipper(0);
    Clipper c;
    c.ptr = clipper;
    return c;
}

void clipper_preserve_collinear(Clipper c, bool v) {
    c.ptr->PreserveCollinear(v);
}

void clipper_reverse_solution(Clipper c, bool v) {
    c.ptr->ReverseSolution(v);
}

void clipper_strictly_simple(Clipper c, bool v) {
    c.ptr->StrictlySimple(v);
}

void clipper_add_path(Clipper c, Path path, ClipperLib::PolyType poly_type, bool closed) {
    c.ptr->AddPath(path_to_clipper(path), poly_type, closed);
}

void clipper_add_paths(Clipper c, Paths paths, ClipperLib::PolyType poly_type, bool closed) {
    c.ptr->AddPaths(paths_to_clipper(paths), poly_type, closed);
}

void clipper_clear(Clipper c) {
    c.ptr->Clear();
}

ClipperLib::IntRect clipper_get_bounds(Clipper c) {
    return c.ptr->GetBounds();
}

ExecutePathsResult clipper_execute_paths(
        Clipper c,
        ClipperLib::ClipType clip_type,
        ClipperLib::PolyFillType subj_fill_type,
        ClipperLib::PolyFillType clip_fill_type
) {
    ClipperLib::Paths out;
    bool success = c.ptr->Execute(clip_type, out, subj_fill_type, clip_fill_type);
    ExecutePathsResult result = {
            .success = success,
            .paths = paths_from_clipper(out)
    };
    return result;
}

ExecutePolyTreeResult clipper_execute_poly_tree(
        Clipper c,
        ClipperLib::ClipType clip_type,
        ClipperLib::PolyFillType subj_fill_type,
        ClipperLib::PolyFillType clip_fill_type
) {
    ClipperLib::PolyTree out;
    bool success = c.ptr->Execute(clip_type, out, subj_fill_type, clip_fill_type);
    ClipperLib::Paths out_open;
    ClipperLib::OpenPathsFromPolyTree(out, out_open);
    ClipperLib::Paths out_closed;
    ClipperLib::ClosedPathsFromPolyTree(out, out_closed);
    ExecutePolyTreeResult result = {
            .success = success,
            .open_paths = paths_from_clipper(out_open),
            .closed_paths = paths_from_clipper(out_closed),
    };
    return result;
}

void free_clipper(Clipper c) {
    delete c.ptr;
}

ClipperOffset make_clipper_offset(double miter_limit, double round_precision) {
    ClipperLib::ClipperOffset *clipper = new ClipperLib::ClipperOffset(miter_limit, round_precision);
    ClipperOffset c;
    c.ptr = clipper;
    return c;
}


void clipper_offset_add_path(ClipperOffset c, Path path, ClipperLib::JoinType join_type, ClipperLib::EndType end_type) {
    c.ptr->AddPath(path_to_clipper(path), join_type, end_type);
}

void clipper_offset_add_paths(
        ClipperOffset c,
        Paths paths,
        ClipperLib::JoinType join_type,
        ClipperLib::EndType end_type
) {
    c.ptr -> AddPaths(paths_to_clipper(paths), join_type, end_type);
}

void clipper_offset_clear(ClipperOffset c) {
    c.ptr->Clear();
}

Paths clipper_offset_execute_paths(ClipperOffset c, double delta) {
    ClipperLib::Paths out;
    c.ptr->Execute(out, delta);
    return paths_from_clipper(out);
}

ExecuteOffsetPolyTreeResult clipper_offset_execute_polytree(ClipperOffset c, double delta) {
    ClipperLib::PolyTree out;
    c.ptr->Execute(out, delta);
    ClipperLib::Paths out_open;
    ClipperLib::OpenPathsFromPolyTree(out, out_open);
    ClipperLib::Paths out_closed;
    ClipperLib::ClosedPathsFromPolyTree(out, out_closed);
    ExecuteOffsetPolyTreeResult result = {
            .open_paths = paths_from_clipper(out_open),
            .closed_paths = paths_from_clipper(out_closed),
    };
    return result;
}

void free_clipper_offset(ClipperOffset c) {
    delete c.ptr;
}