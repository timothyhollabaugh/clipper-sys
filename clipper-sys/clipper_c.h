//
// Created by tim on 6/18/20.
//

#ifndef CLIPPER_SYS_WRAPPER_H
#define CLIPPER_SYS_WRAPPER_H

#include <stdint.h>

#include "clipper/cpp/clipper.hpp"

/// A collection of contiguous points in memory
/// for interfacing with Rust.
///
/// A `Path` must not outlive the memory that it references.
typedef struct {
    ClipperLib::IntPoint *ptr;
    size_t len;
} Path;

typedef struct {
    Path *ptr;
    size_t len;
} Paths;

typedef struct {
    ClipperLib::Clipper *ptr;
} Clipper;

typedef struct {
    ClipperLib::ClipperOffset *ptr;
} ClipperOffset;

typedef struct {
    bool success;
    Paths paths;
} ExecutePathsResult;

typedef struct {
    bool success;
    Paths open_paths;
    Paths closed_paths;
} ExecutePolyTreeResult;

typedef struct {
    Paths open_paths;
    Paths closed_paths;
} ExecuteOffsetPolyTreeResult;

void free_path(Path path);

void free_paths(Paths path);

Path test_path_roundtrip(Path path);

Paths test_paths_roundtrip(Paths paths);

double area(Path path);

bool orientation(Path poly);

int point_in_polygon(ClipperLib::IntPoint pt, Path path);

Path reverse_path(Path p);

Paths reverse_paths(Paths p);

Path clean_polygon(Path path, double distance);

Paths clean_polygons(Paths paths, double distance);

Paths simplify_polygon(Path poly, ClipperLib::PolyFillType filltype);

Paths simplify_polygons(Paths polys, ClipperLib::PolyFillType filltype);

Paths minkowski_sum(Path pattern, Path path, bool path_is_closed);

Paths minkowski_sums(Path pattern, Paths paths, bool path_is_closed);

Paths minkowski_diff(Path poly1, Path poly2);

Clipper make_clipper();

void clipper_preserve_collinear(Clipper c, bool v);

void clipper_reverse_solution(Clipper c, bool v);

void clipper_strictly_simple(Clipper c, bool v);

void clipper_add_path(Clipper c, Path path, ClipperLib::PolyType poly_type, bool closed);

void clipper_add_paths(Clipper c, Paths paths, ClipperLib::PolyType poly_type, bool closed);

void clipper_clear(Clipper c);

ClipperLib::IntRect clipper_get_bounds(Clipper c);

ExecutePathsResult clipper_execute_paths(
        Clipper c,
        ClipperLib::ClipType clip_type,
        ClipperLib::PolyFillType subj_fill_type,
        ClipperLib::PolyFillType clip_fill_type
);

ExecutePolyTreeResult clipper_execute_poly_tree(
        Clipper c,
        ClipperLib::ClipType clip_type,
        ClipperLib::PolyFillType subj_fill_type,
        ClipperLib::PolyFillType clip_fill_type
);

void free_clipper(Clipper c);

ClipperOffset make_clipper_offset(double miter_limit, double round_precision);

void clipper_offset_add_path(ClipperOffset c, Path path, ClipperLib::JoinType join_type, ClipperLib::EndType end_type);

void clipper_offset_add_paths(
        ClipperOffset c,
        Paths paths,
        ClipperLib::JoinType join_type,
        ClipperLib::EndType end_type
);

void clipper_offset_clear(ClipperOffset c);

Paths clipper_offset_execute_paths(ClipperOffset c, double delta);

ExecuteOffsetPolyTreeResult clipper_offset_execute_polytree(ClipperOffset c, double delta);

void free_clipper_offset(ClipperOffset c);
#endif //CLIPPER_SYS_WRAPPER_H
